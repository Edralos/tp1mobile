package com.example.tp1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.view.View;

import com.example.tp1.animals.Animal;
import com.example.tp1.animals.AnimalList;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

public class MainActivity extends AppCompatActivity {

    String[] list =  AnimalList.getNameArray();


    @Override
    protected void onCreate(final Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        final RecyclerView rv = (RecyclerView)findViewById(R.id.nameList);

        rv.setLayoutManager(new LinearLayoutManager((this)));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new AnimalListAdapter());


    }


    class AnimalListAdapter extends RecyclerView.Adapter<RowHolder> {
        @NonNull
        @Override
        public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return(new RowHolder(getLayoutInflater().inflate(R.layout.row, parent, false)));

        }

        @Override
        public void onBindViewHolder(@NonNull RowHolder holder, int position) {

            holder.bindModel(list[position], getResources().getIdentifier(AnimalList.getCountry(list[position]).getImgFile(), "drawable", getPackageName()));
        }

        @Override
        public int getItemCount() {
            return list.length;
        }
    }


     class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        ImageView icon;

        public RowHolder(View row) {
            super(row);
            name = (TextView)row.findViewById(R.id.name);
            icon = (ImageView)row.findViewById(R.id.iconRow);
            row.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            String animal = name.getText().toString();
            Intent intent = new Intent(MainActivity.this, AnimalActivity.class);
            intent.putExtra("animal",animal);
            startActivity(intent);

        }

        public void bindModel(String item, int imageId) {
            name.setText(item);
            icon.setImageResource(imageId);


        }
    }

}
