package com.example.tp1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tp1.animals.Animal;
import com.example.tp1.animals.AnimalList;

import java.util.HashMap;

public class AnimalActivity extends AppCompatActivity {

    final int ANIMAL_ACTIVITY_ENDED = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);
        Intent intent = getIntent();
        String animalName = intent.getStringExtra("animal");

        final Animal ANIMAL = AnimalList.getCountry(animalName);
        final HashMap<String, TextView> MAP = new HashMap<String, TextView>();

        MAP.put("esperance",(TextView)findViewById(R.id.esperance_field));
        MAP.put("gestation",(TextView)findViewById(R.id.gestation_field));
        MAP.put("naissance",(TextView)findViewById(R.id.naissance_field));
        MAP.put("adulte",(TextView)findViewById(R.id.adult_field));
        MAP.put("statut",(TextView)findViewById(R.id.statut_field));

        MAP.get("esperance").setText(ANIMAL.getStrHightestLifespan());
        MAP.get("gestation").setText(ANIMAL.getStrGestationPeriod());
        MAP.get("naissance").setText(ANIMAL.getStrBirthWeight());
        MAP.get("adulte").setText(ANIMAL.getStrAdultWeight());
        MAP.get("statut").setText(ANIMAL.getConservationStatus());
        ImageView imageAnimal = (ImageView)findViewById(R.id.animalImage);


        int id = getResources().getIdentifier(ANIMAL.getImgFile(),"drawable",getPackageName());
        imageAnimal.setImageResource(id);

        Button saveButton = (Button)findViewById(R.id.save);
        saveButton.setOnClickListener(new  Button.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                ANIMAL.setConservationStatus(MAP.get("statut").getText().toString());
            }
        });
    }



}
